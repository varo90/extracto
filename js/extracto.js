$(document).ready(function() {
    $("#textinput").on('keyup', function (e) {
        if (e.keyCode == 13) {
            $("#searchbutton").click();
        }
    });
      
    $('#searchbutton').on('click',function (e) {
        get_tickets($("#textinput").val());
    });
});

function get_tickets(reference) {
    if(reference == 0) {
        var msg = '<h4><img src="images/busy.gif" /> Generando extracto...</h4>';
    } else {
        var msg = '<h4><img src="images/busy.gif" /> Generando extracto del cliente '+reference+'</h4>';
    }
    bootbox.dialog({
        title: "Cabeceras",
        message: "¿Mostrar encabezado y pie?",
        buttons: {
            noprint: {
                label: "No",
                className: 'btn-warning',
                callback: function(){
                    window.open("scripts/generar_extracto.php?client="+reference+"&cabecera=n","_blank")
                }
            },
            print: {
                label: "S&iacute;",
                className: 'btn-info',
                callback: function(){
                    window.open("scripts/generar_extracto.php?client="+reference+"&cabecera=y","_blank")
                }
            }
        }
    });
}