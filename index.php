<?php include('header.php'); ?>
    <div class="container">


      <!-- Form Name -->
      <legend>Generar extracto</legend>
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label">Código de cliente:</label>  
        <div class="col-md-4">
        <input id="textinput" name="textinput" type="text" placeholder="Introduce el código de cliente" class="form-control input-md" required="">
          
        </div>
      </div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="searchbutton"></label>
      <div class="col-md-4">
        <button id="searchbutton" name="searchbutton" class="btn btn-primary">Generar extracto</button>
      </div>
    </div>
    <br><br>
    <div  id ="result" style="display:none"></div>

</body>
</html>