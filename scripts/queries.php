<?php

class queries {

    static function get_listado_eventos() {
        $sql = "SELECT * FROM eventList ORDER BY id DESC";
        return $sql;
    }

    static function get_tickets(){
        $sql="SELECT t0.U_GSP_CACLIE AS client,
            t0.U_GSP_CANUME AS Transaccions,
            CONCAT(CONVERT(VARCHAR(10), FORMAT(t0.U_GSP_CADATA, 'dd-MM-yyyy'), 105),' ',t0.U_GSP_CAHORA) AS create_date,
            t0.[U_GSP_SEIMPPEND] AS pendiente,
            t0.U_GSP_CATOTA AS total_amt,
            t1.U_GSP_LIQUAN AS quantity,
            t1.U_GSP_LIPREU AS priceunit,
            t1.U_GSP_LIIMPR AS pricetot,
            t1.U_GSP_LIDES1 AS description,
            t1.U_GSP_SEADJPRICE AS arreglo
        FROM [dbo].[@gsp_tpvcap] t0 WITH (NOLOCK)
            LEFT JOIN [dbo].[@gsp_tpvlin] t1 WITH (NOLOCK) ON t0.code = t1.u_gsp_doccode
        WHERE t0.U_GSP_SEIMPPEND <> 0 AND t0.U_GSP_CACLIE = ? AND t0.u_gsp_cadocu <>'VENTATEMP' AND t0.u_gsp_caesta = 'X' AND U_GSP_SEFDOCENTRY is NULL AND t1.U_GSP_LIDES1 <> 'Total Arreglos'
        ORDER BY t0.U_GSP_CADATA DESC, t0.U_GSP_CAHORA DESC";
        return $sql;
    }

    static function get_pendiente(){
        $sql="SELECT t0.U_GSP_CACLIE AS client,
                SUM(t0.[U_GSP_SEIMPPEND]) AS pendiente,
                SUM(t0.U_GSP_CATOTA) AS total_amt
            FROM [dbo].[@gsp_tpvcap] t0 WITH (NOLOCK)
            WHERE t0.U_GSP_SEIMPPEND <> 0 AND t0.U_GSP_CACLIE = ? AND t0.u_gsp_cadocu <>'VENTATEMP' AND t0.u_gsp_caesta = 'X' and U_GSP_SEFDOCENTRY is NULL
            GROUP BY t0.U_GSP_CACLIE";
        return $sql;
    }

    static function get_cobros() {
        $sql="SELECT pay.U_GSP_CANUME as bill_num,
                    CONCAT(CONVERT(VARCHAR(10), FORMAT(pay.U_GSP_CADATA, 'dd-MM-yyyy'), 105),' ',pay.U_GSP_CAHORA) AS create_date,
                    pay.U_GSP_CATOTA as total_amt,
                    pay.U_GSP_CACLIE as clientnbr,
                    pay.U_GSP_DESC collate Modern_Spanish_CI_AS as description,
                    pay.U_GSP_SEIMPPEND as pendiente,
                    sel.SlpName collate Modern_Spanish_CI_AS as seller,
                    despay.PayMethCod as codepago,
                    despay.Descript as tipopago
            FROM SBO_EULALIA.dbo.[@GSP_TPVPAYMENT] pay with (NOLOCK)
            LEFT JOIN SBO_EULALIA.dbo.[OPYM] despay with (NOLOCK) ON pay.U_GSP_CAFPAG = despay.PayMethCod
            LEFT JOIN SBO_EULALIA.dbo.[OSLP] sel with (NOLOCK) ON pay.U_GSP_CAVENE = sel.SlpCode
            WHERE pay.U_GSP_CACLIE = ? AND pay.U_GSP_SEIMPPEND <> 0";
        return $sql;
    }
    
    static function get_cliente() {
        $sql="SELECT TOP 1 treatment.Name collate Modern_Spanish_CI_AS as treatment,
                    client.CardCode AS CardCode,
                    client.CardName collate Modern_Spanish_CI_AS as CardName,
                    client.Block collate Modern_Spanish_CI_AS as Block,
                    client.Address collate Modern_Spanish_CI_AS as Address,
                    client.ZipCode,
                    client.City collate Modern_Spanish_CI_AS as City,
                    client.Country as CountryCode,
                    client.LicTradNum as cif,
                    ctry.Name collate Modern_Spanish_CI_AS as CountryName,
                    treatment.Code AS code_treatment,
                    client.Phone1 as phone1,
                    client.Phone2 as phone2,
                    client.Cellular as cellular,
                    client.U_GSP_SEX AS gendre
            FROM dbo.OCRD client with (NOLOCK)
            LEFT JOIN dbo.[@GSP_SETREATMENT] treatment with (NOLOCK) ON client.U_GSP_TREATMENT = treatment.Code
            LEFT JOIN OCRY ctry with (NOLOCK) ON client.Country = ctry.Code
            WHERE CardCode=?";
        return $sql;
    }

    static function get_clients() {
        $sql = "SELECT * FROM eventContent WHERE eventId = ? AND status = ?";
        return $sql;
    }
    
    static function get_vendedores($array_of_clients) {
        $sql = "SELECT DISTINCT emp.SlpCode AS codemp, emp.SlpName AS nomemp, cli.CardCode cliente
                FROM OCRD cli
                LEFT JOIN OSLP emp ON cli.SlpCode = emp.SlpCode
                WHERE cli.CardCode IN ($array_of_clients)
                --GROUP BY emp.SlpCode, emp.SlpName--
                ORDER BY emp.SlpCode";
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}
