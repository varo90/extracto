<?php

require('../fpdf/WriteHTML.php');
define("INTERLIN", 5);
define('EURO',chr(128));

class PDF extends FPDF
{
    var $client = '';
    var $cabecera = 'n';
    var $num_page = 0;
    var $header_called;
    var $font_type = 'Helvetica';
    var $font_size = 11;
    var $x = 2;
    var $y = 40;
    var $yl = 78;
    var $total_compras = 0;
    var $total_cobros = 0;

    function set_client($client,$cabecera) {
        $this->client = $client;
        $this->cabecera = $cabecera;
    }
    
    //Cabecera de página
    function Header()
    {
        if($this->cabecera == 'y') {
            $this->salto(7);
            $this->SetFont('Arial','',12);
            $this->Cell(0,$this->height,'SANTA EULALIA',0,0,'C');
            $this->salto(5);
            $this->SetFont('Arial','',6);
            $this->Cell(0,$this->height,'BARCELONA 1843',0,0,'C');
            $this->salto(10);
        } else {
            $this->salto(22);
        }
        // if($this->num_page == 1) {
            $this->SetFont($this->font_type,'',$this->font_size);
            $this->Cell(158,30,utf8_decode(date('d/m/Y')),0,0,R);
            $this->salto(INTERLIN*3);
            if($this->client->gendre == 'H') {
                $det = ' del ';
            } else if($this->client->gendre == 'H') {
                $det = ' de la ';
            } else {
                $det = ' de ';
            }
            $this->Cell(90,30,'Extracto' . $det . $this->client->treatment . utf8_decode($this->client->CardName));

            $this->num_page++;
        // }
        // $this->dist_items = 0;
    }
    
    function Footer() {
        if($this->cabecera == 'y') {
            $this->SetY(-17);
            $this->Cell($this->x);
            $this->SetFont('Helvetica','',7);
            $this->Cell(0,$this->height,utf8_decode('PASSEIG DE GRÀCIA 93  08008 BARCELONA  T+34 932 150 674'),0,0,'C');
            $this->salto(3.5);
            $this->Cell(0,$this->height,'WWW.SANTAEULALIA.COM  SE@SANTAEULALIA.COM',0,0,'C');
            $this->salto(7);
            $this->SetFont('Helvetica','',5);
            $this->Cell(0,$this->height,'SANTA EULALIA SA  REGISTRO MERCANTIL DE BARCELONA  H 18630  F195  T658  L 204  S 2  NIF A-08.031734',0,0,'C');
        }
    }

    function print_tickets($tickets,$pendiente,$columns) {
        $this->salto(INTERLIN*6);
        $this->SetFont($this->font_type,'',$this->font_size);
        $this->salto(0);
        // $this->Cell($this->x);
        $this->print_header_table($columns);
        
        $this->Line(30,$this->yl,190,$this->yl);
        $this->salto(INTERLIN*4);
        $this->yl += 3;
        $this->SetFont($this->font_type,'',$this->font_size);

        $total = 0;
        $cont = 0;
        foreach($tickets as $cont => $ticket) {
            if($cont > 0) {
                $this->salto(INTERLIN);
            }
            $this->yl += INTERLIN;
            foreach($columns as $column => $lenght) {
                if($column == 'FECHA') {
                    $fecha = date('d-m-Y',strtotime($ticket->create_date));
                    $this->Cell($lenght,5,$fecha,'');
                } else if($column == 'DESCRIPCIÓN') {
                    $descripcion = str_replace('00 UNIC','',utf8_decode($ticket->description));
                    $descripcion = str_replace('UNIC','',$descripcion);
                    $this->Cell($lenght,5,$descripcion,'');
                } else if($column == 'UDS.') {
                    $uds = round($ticket->quantity);
                    $this->Cell($lenght,5,$uds,'');
                } else if($column == 'PRECIO') {
                    $priceunit = number_format((float)$ticket->priceunit, 2, ',', '') . ' ' . EURO;
                    if (strpos($ticket->description, 'Arreglo') !== false) {
                        $priceunit = number_format((float)$ticket->arreglo, 2, ',', '') . ' ' . EURO;
                    }
                    $this->Cell($lenght,5,$priceunit,0,0,'R');
                } else if($column == 'TOTAL') {
                    if (strpos($ticket->description, 'Arreglo') !== false) {
                        $totalline = number_format((float)$ticket->arreglo, 2, ',', '') . ' ' . EURO;
                        $total += $ticket->arreglo;
                    } else {
                        $totalline = number_format((float)$ticket->pricetot, 2, ',', '') . ' ' . EURO;
                        $total += $ticket->pricetot;
                    }
                    $this->Cell($lenght,5,$totalline,0,0,'R');
                }
            }
            // AÑADIR LÍNEA CON ARREGLO SI LO HAY
            if(strpos($ticket->description, 'Arreglo') === false && $ticket->arreglo != 0) {
                $this->salto(INTERLIN);
                $this->yl += INTERLIN;
                $this->Cell($columns['FECHA'],5,$fecha,'');
                $this->Cell($columns['DESCRIPCIÓN'],5,'Arreglo ' . $descripcion,'');
                $this->Cell($columns['UDS.'],5,$uds,'');
                $this->Cell($columns['PRECIO'],5,$priceunit,'');
                $this->Cell($columns['TOTAL'],5,$totalline,'');
            }
        }
        $adelantos = $pendiente->pendiente - $total;
        if($pendiente != false && $adelantos != 0) {
            $total = $total + $adelantos;
            $this->salto(INTERLIN);
            $this->yl += INTERLIN;
            $this->Cell($columns['FECHA'],5,'','');
            $this->Cell($columns['DESCRIPCIÓN'],5,utf8_decode('Abonado anteriormente'),'');
            $this->Cell($columns['UDS.'],5,1,'');
            $this->Cell($columns['PRECIO'],5,number_format((float)$adelantos, 2, ',', '') . ' ' . EURO,0,0,'R');
            $this->Cell($columns['TOTAL'],5,number_format((float)$adelantos, 2, ',', '') . ' ' . EURO,0,0,'R');
        }
        $this->Line(30,$this->yl,190,$this->yl);
        $this->SetFont($this->font_type,'',$this->font_size);
        if(count($tickets) > 1) {
            $this->salto(INTERLIN);
        }
        $this->salto(2);
        $this->Cell(50,5,'TOTAL COMPRAS','');
        $total_str = number_format((float)$total, 2, ',', '') . ' ' . EURO;
        $this->Cell(110,5,$total_str,0,0,'R');
        $this->total_compras = $total;
    }

    function print_cobros($cobros,$columns) {
        $this->salto(INTERLIN*3);
        $this->salto(-INTERLIN+1);
        $this->yl += INTERLIN*6;
        $this->print_header_table($columns);
        $this->Line(30,$this->yl,190,$this->yl);

        $this->SetFont($this->font_type,'',$this->font_size);
        $total = 0;
        $cont = 0;
        $this->salto((INTERLIN*4)-1);
        foreach($cobros as $cont => $cobro) {
            if($cont > 0) {
                $this->salto(INTERLIN);
            }
            $this->yl += INTERLIN;
            foreach($columns as $column => $lenght) {
                if($column == 'FECHA') {
                    $fecha = date('d-m-Y',strtotime($cobro->create_date));
                    $this->Cell($lenght,5,$fecha,'');
                } else if($column == 'FORMA DE PAGO') {
                    $this->Cell($lenght,5,$this->get_tipopago($cobro->codepago,$cobro->tipopago),'');
                } else if($column == 'IMPORTE') {
                    $this->Cell($lenght,5,number_format((float)$cobro->total_amt, 2, ',', '') . ' ' . EURO,0,0,'R');
                    $total += $cobro->total_amt;
                }
            }
        }
        $this->yl += 2;
        $this->Line(30,$this->yl,190,$this->yl);
        $this->SetFont($this->font_type,'',$this->font_size);
        if(count($cobros) > 0) {
            $this->salto(INTERLIN);
        }
        $this->salto(2);
        $this->Cell(50,5,'TOTAL COBRADO','');
        $total_str = number_format((float)$total, 2, ',', '') . ' ' . EURO;
        $this->Cell(110,5,$total_str,0,0,'R');
        $this->total_cobros = $total;
    }

    function print_extracto() {
        $this->salto(INTERLIN*3);
        $total = $this->total_compras - $this->total_cobros;
        $extracto = number_format((float)$total, 2, ',', '') . ' ' . EURO;
        $this->Cell(50,5,'TOTAL PENDIENTE','');
        $this->Cell(110,5,$extracto,0,0,'R');
    }

    function print_header_table($columns) {
        $this->SetFont($this->font_type,'',$this->font_size);
        $cont = 0;
        foreach($columns as $column => $lenght) {
            $cont++;
            if($cont > 2) {
                $this->Cell($lenght,30,utf8_decode($column),0,0,'R');
            } else {
                $this->Cell($lenght,30,utf8_decode($column),'');
            }
        }
    }

    function get_tipopago($key_payment,$name_payment) {
        $transferencia = ['CL_ACUENTA','CL_AJCTA','CL_TRANSFER'];
        $tarjeta = ['CL_AMERICAN','CL_BBVA','CL_CAIXA_CAT','CL_CAIXA_CAT_EX','CL_DINERS','CL_GBP_CCARD','CL_PAYGOLD','CL_SANTANDER','CL_VISA_CAIXA','CLCREDCARD_WEB'];
        $efectivo = ['CL_EFECTIU'];
        $cheque = ['CL_TALO'];

        if(in_array($key_payment,$transferencia)) {
            return 'Transferencia';
        } else if(in_array($key_payment,$tarjeta)) {
            return 'Tarjeta';
        } else if(in_array($key_payment,$efectivo)) {
            return 'Efectivo';
        } else if(in_array($key_payment,$cheque)) {
            return 'Cheque';
        } else {
            return utf8_decode($name_payment);
        }
    }

    function separate_in_rows($str) {
        $rows = array();
        $max_lenght = $this->max_lenght;
        $long_str = strlen($str);
        if($long_str > $max_lenght) {
            while($str[$max_lenght] != ' ') {
                $max_lenght--;
            }
            $rows[] = $str_line_1 = substr($str, 0, $max_lenght);
            $str_line_2 = substr($str, $max_lenght+1, $long_str);
            
            if($str_line_2 != ' ' && $str_line_2 != NULL) {
                $rows[] = $str_line_2;
            }
        } else {
            $rows[] = $str;
        }
        return $rows;
    }

    function nothing_to_show($message) {
        $this->SetFont($this->font_type,'',$this->font_size);
        $this->Cell(50,5,$message,'');
        $this->Cell(110,5,'0,00 '.EURO,0,0,'R');
    }

    function salto($interlin) {
        $this->Ln($interlin);
        $this->Cell($this->x+10);
    }
}