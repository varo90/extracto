<?php

error_reporting(E_ALL);
ini_set('display_errors', 'Off');

    include('db_connections.php');
    include('queries.php');
    include('show_pdf.php');

    $db_ms = new db();
    $client = $_GET['client'];
    $client_data = $db_ms->make_query(queries::get_cliente(),[$client])[0];
    $tickets = $db_ms->make_query(queries::get_tickets(),[$client]);
    $tickets = (count($tickets) > 0) ? $tickets : false;
    $pendientes = $db_ms->make_query(queries::get_pendiente(),[$client]);
    $pendiente = (count($pendientes) > 0) ? $pendientes[0] : false;
    $cobros = $db_ms->make_query(queries::get_cobros(),[$client]);
    $cobros = (count($cobros) > 0) ? $cobros : false;
    unset($db_ms);
    
    $pdf=new PDF();
    $pdf->set_client($client_data,$_GET['cabecera']);
    $pdf->AliasNbPages();
    //Primera página
    $pdf->AddPage();
    $pdf->SetY(30);
    if(count($tickets) > 0 && $tickets != false) {
        $columns = ['FECHA'=>22,'DESCRIPCIÓN'=>88,'UDS.'=>4,'PRECIO'=>24,'TOTAL'=>22];
        $pdf->print_tickets($tickets,$pendiente,$columns);
    } else {
        $pdf->salto(INTERLIN*6+4);
        $pdf->nothing_to_show('COMPRAS');
    }
    if(count($cobros) > 0 && $cobros != false) {
        $columns = ['FECHA'=>37,'FORMA DE PAGO'=>118,'IMPORTE'=>5];
        $pdf->print_cobros($cobros,$columns);
    } else {
        $pdf->salto(INTERLIN*3);
        $pdf->nothing_to_show('COBROS');
    }
    $pdf->print_extracto();

    $pdf->Output();