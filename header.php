<!DOCTYPE html>
<html>
<head>
    <title>Extractos Santa Eulalia</title>
    <link href='css/extracto.css' rel='stylesheet' type='text/css'>
    <link href='css/simple-images.css' rel='stylesheet' type='text/css'>
    <link href='css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery.lazy.min.js" type="text/javascript"></script>
    <script src="js/extracto.js" type="text/javascript"></script>
    <script src="js/jquery.imagebox.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.blockUI.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    </nav>